import argparse
from typing import Optional

from attrs import asdict, define

ArgParseParameters = list[dict]


@define
class Arguments:
    parsed_arguments: Optional[dict] = None

    def lookup_raw_value(self, name: str):
        assert self.parsed_arguments
        return self.parsed_arguments[name]


@define
class Parameters:
    def construct_argparse_parameters(self) -> ArgParseParameters:
        args_list: ArgParseParameters = []
        for arg_name in asdict(self).keys():
            args_list += getattr(self, arg_name).parameters()
        return args_list

    def parse_command_line(self, app_description: str):
        application_arguments = self.construct_argparse_parameters()
        parser = create_arg_parser(app_description, application_arguments)
        args_namespace = parser.parse_args()
        args_dict = dict(vars(args_namespace))
        args_dict = {
            parameter_name_from_argparse_name(key): value
            for key, value in args_dict.items()
        }
        for param_name in asdict(self).keys():
            parameter = getattr(self, param_name)
            parameter.get_arguments().parsed_arguments = args_dict


@define
class OptionalFlag:
    name: str
    help: str
    arguments: Arguments = Arguments()

    def parameters(self) -> ArgParseParameters:
        return [
            {
                "name": f"--{argparse_name_from_parameter_name(self.name)}",
                "help": self.help,
                "action": "store_true",
            }
        ]

    def is_set(self) -> bool:
        return bool(self.arguments.lookup_raw_value(self.name))

    def get_arguments(self) -> Arguments:
        return self.arguments


@define
class OptionalStr:
    name: str
    help: str
    default: str
    arguments: Arguments = Arguments()

    def parameters(self) -> ArgParseParameters:
        return [
            {
                "name": f"--{argparse_name_from_parameter_name(self.name)}",
                "type": str,
                "default": self.default,
                "help": f"{self.help} (default {self.default})",
            }
        ]

    def get_value(self) -> str:
        return self.arguments.lookup_raw_value(self.name)


@define
class OptionalStrList:
    name: str
    help: str
    arguments: Arguments = Arguments()

    def parameters(self) -> ArgParseParameters:
        return [
            {
                "name": f"--{argparse_name_from_parameter_name(self.name)}",
                "help": self.help,
                "nargs": "?",
                "default": [],
                "type": list[str],
            }
        ]

    def get_value(self) -> list[str]:
        val = self.arguments.lookup_raw_value(self.name)
        val = "".join(val).split(",")
        if val is None or val == "" or val == [""]:
            return []
        return val

    def get_arguments(self) -> Arguments:
        return self.arguments


@define
class OptionalInt:
    name: str
    help: str
    default: int
    arguments: Arguments = Arguments()

    def parameters(self) -> ArgParseParameters:
        return [
            {
                "name": f"--{argparse_name_from_parameter_name(self.name)}",
                "type": int,
                "default": self.default,
                "help": f"{self.help} (default {self.default})",
            }
        ]

    def get_value(self) -> int:
        return int(self.arguments.lookup_raw_value(self.name))

    def get_arguments(self) -> Arguments:
        return self.arguments


@define
class OptionalFloat:
    name: str
    help: str
    default: float
    arguments: Arguments = Arguments()

    def parameters(self) -> ArgParseParameters:
        return [
            {
                "name": f"--{argparse_name_from_parameter_name(self.name)}",
                "type": float,
                "default": self.default,
                "help": f"{self.help} (default {self.default})",
            }
        ]

    def get_value(self) -> float:
        return float(self.arguments.lookup_raw_value(self.name))

    def get_arguments(self) -> Arguments:
        return self.arguments


@define
class OptionalChoice:
    name: str
    help: str
    choices: list[str]
    default: str | None
    arguments: Arguments = Arguments()

    def parameters(self) -> ArgParseParameters:
        if self.default is None:
            return [
                {
                    "name": f"--{argparse_name_from_parameter_name(self.name)}",
                    "choices": self.choices,
                    "required": True,
                    "help": self.help,
                }
            ]
        else:
            return [
                {
                    "name": f"--{argparse_name_from_parameter_name(self.name)}",
                    "choices": self.choices,
                    "default": self.default,
                    "help": f"{self.help} (default {self.default})",
                }
            ]

    def get_value(self) -> str:
        return self.arguments.lookup_raw_value(self.name)

    def get_arguments(self) -> Arguments:
        return self.arguments


@define
class RequiredStr:
    name: str
    help: str
    arguments: Arguments = Arguments()

    def parameters(self) -> ArgParseParameters:
        return [
            {
                "name": argparse_name_from_parameter_name(self.name),
                "help": self.help,
            }
        ]

    def get_value(self) -> str:
        return self.arguments.lookup_raw_value(self.name)

    def get_arguments(self) -> Arguments:
        return self.arguments


@define
class RequiredStrList:
    name: str
    help: str
    arguments: Arguments = Arguments()

    def parameters(self) -> ArgParseParameters:
        return [
            {
                "name": argparse_name_from_parameter_name(self.name),
                "help": self.help,
                "nargs": "+",
                "type": list[str],
            }
        ]

    def get_value(self) -> list[str]:
        val = self.arguments.lookup_raw_value(self.name)
        val = ["".join(chars) for chars in val]
        return val

    def get_arguments(self) -> Arguments:
        return self.arguments


@define
class RequiredConfigFilename:
    impl: RequiredStr = RequiredStr(
        name="config_filename",
        help="config filename to read",
    )

    def parameters(self) -> ArgParseParameters:
        return self.impl.parameters()

    def get_value(self) -> str:
        return self.impl.get_value()

    def get_arguments(self) -> Arguments:
        return self.impl.arguments


@define
class OptionalClientName:
    impl: OptionalChoice = OptionalChoice(
        name="client_name",
        help="client name",
        default="TestFD",
        choices=["TestFD", "ValpoFD"],
    )

    def parameters(self) -> ArgParseParameters:
        return self.impl.parameters()

    def get_value(self) -> str:
        return self.impl.get_value()

    def get_arguments(self) -> Arguments:
        return self.impl.arguments


@define
class OptionalDaysAgo:
    impl: OptionalInt = OptionalInt(
        name="days_ago",
        help="retrieve incidents this number of days before today",
        default=1,
    )

    def parameters(self) -> ArgParseParameters:
        return self.impl.parameters()

    def get_value(self) -> int:
        return self.impl.get_value()

    def get_arguments(self) -> Arguments:
        return self.impl.arguments


# @define
# class UnderlyingSymbols:
#     default: list[str] | None = None
#     name: str = "underlying_symbols"
#     help: str = "underlying symbols to query (comma-separated) ex: BTC,ETH"

#     def parameters(self) -> ArgParseParameters:
#         if self.default is None:
#             return [
#                 {
#                     name=f"--{argparse_name_from_parameter_name(self.name)}",
#                     required=True,
#                     help=self.help,
#                 }
#             ]
#         else:
#             return [
#                 {
#                         name=f"--{argparse_name_from_parameter_name(self.name)}",
#                         default=",".join(self.default),
#                         help=self.help,
#                 }
#             ]

#     def get_value(self) -> list[str]:
#         symbols = getattr(self, "parsed_arguments").lookup_raw_value(self.name)
#         if symbols is None or symbols == "":
#             return []
#         return symbols.split(",")


# @define
# class OptionSymbols:
#     default: list[str]
#     name: str = "option_symbols"
#     help: str = (
#         "option symbols to query (comma-separated) ex: BTC-7APR23-27000-P,ETH-29DEC23-1500-P"
#     )

#     def parameters(self) -> ArgParseParameters:
#         return [
#             {
#                 name=f"--{argparse_name_from_parameter_name(self.name)}",
#                 default=",".join(self.default),
#                 help=self.help,
#             }
#         ]

#     def get_value(self) -> list[str]:
#         symbols = getattr(self, "parsed_arguments").lookup_raw_value(self.name)
#         if symbols is None or symbols == "":
#             return []
#         return symbols.split(",")


# @define
# class Verbose:
#     impl: OptionalFlag = OptionalFlag(
#         name="verbose",
#         help="show all messages, even protocol-level messages",
#     )

#     def parameters(self) -> ArgParseParameters:
#         return self.impl.parameters()

#     def is_set(self) -> bool:
#         return self.impl.is_set(arguments)


def parameter_name_from_argparse_name(arg_name: str) -> str:
    return arg_name.replace("-", "_")


def argparse_name_from_parameter_name(actual_name: str) -> str:
    return actual_name.replace("_", "-")


def create_arg_parser(
    description: str, extra_arguments: list
) -> argparse.ArgumentParser:
    def formatter(prog: str) -> argparse.HelpFormatter:
        return argparse.RawTextHelpFormatter(prog, max_help_position=60)

    parser = argparse.ArgumentParser(description=description, formatter_class=formatter)
    # parser.add_argument(
    #     "--config-file",
    #     help="/path/to/config file to use (or set env var CONFIG_FILE)",
    #     type=str,
    #     required=False,
    # )
    # parser.add_argument(
    #     "--config-json",
    #     help="json configuration string. overrides config from file",
    #     type=str,
    #     required=False,
    # )
    # parser.add_argument(
    #     "--dump-config", help="dump config and exit", action="store_true"
    # )
    for argument in extra_arguments:
        name = argument.pop("name")
        parser.add_argument(name, **argument)
    return parser
